package main

import "fmt"

func say(message string, c chan<- string) {
	c <- message

	c <- "Holis"
	c <- "Cara"
	fmt.Println(len(c), cap(c))

	c <- "De bola"
	close(c)
}

func main() {
	c := make(chan string, 4)
	fmt.Println("Channels")
	go say("Bye", c)

	fmt.Println(<-c) // take out the first message

	fmt.Println("----")
	for message := range c {
		fmt.Println(message) // take out the rest of the messages
	}
}
